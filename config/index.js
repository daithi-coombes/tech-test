import {Command} from 'commander'
import { createRequire } from 'module'
const require = createRequire(import.meta.url)
const packageJSON = require("../package.json")

const program = new Command()
program.version(packageJSON.version)
program
  .option('-d, --debug', 'output extra debugging')
  .option('-p, --pairs <string>', 'array of pairs in format btc-usd,eth-usd,btc-eth...')
  .option('-e, --endpoint <string>', 'the upholde api endpoint, without any paths')
  .option('-i, --interval <number>', 'the interval in milliseconds to check. Note checking too many pairs may blow the stack.')
program.parse(process.argv)

// cli args, env var, config file
const env  = process.env.TT_ENV ?? 'development'
const envConfig = await import(`./${env}.js`)
const Config = {
  env,
  interval: program.opts().interval ?? process.env.TT_INTERVAL ?? envConfig.interval ?? 2000,
  endpoint: program.opts().endpoint ?? process.env.TT_ENDPOINT ?? envConfig.endpoint ?? 'https://api.uphold.com',
  pairs: program.opts().pairs ?? process.env.TT_PAIRS ?? envConfig.pairs ?? 'btc-usd,ETH-USD',
}

export default Config