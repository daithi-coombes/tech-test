# Tech Test

This app will poll the [Uphold API](https://uphold.com/en/developer/api/documentation/#tickers) every x milliseconds.

If the price changes by 0.01% (either up or down), then an alert will be logged to stdout.
To use your own alerts overwrite `./lib/Alert::doAlert` and use as shown in `index.js`.


## Usage

### Configuration

Configuration can be done in the following precedence: `cli args`, `environment variables` and `configuration file` in `config/$env.js`. See `config/development.js` for exmple of the former.

To view a list of cli arguments run:
```
npm start -- --help
```

Configuration files in `json` format are loaded from `./config/${env}.js`. For example to load a production configuration file, create `./config/$production` and then set the environment variable `TT_ENV=production`. The environment will default to `development` if none set.
```javascript
export default {
    interval: 5000,
    endpoint: 'https://api.uphold.com',
}
```

Install dependencies
```
npm install
```
or
```
yarn install
```

### Running
Now run the bot
```
npm start
```


## The Problem
Write a bot that alerts on change of 0.01 percent at specific interval.
Use the UpHold API

## Definition of Done

 - config inc env vars
 - moch unit tests
 - ECMAScript 2020

