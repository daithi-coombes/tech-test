import assert from 'assert'
import Poller from '../../lib/Poller.mjs'

describe('Poller', function(){

  let underTest

  beforeEach(()=>{
    const mockClient = { getTicker: ()=> Promise.resolve({bid: 10.01 }) }
    underTest = new Poller(['foo-bar','biz-baz'], mockClient)
    underTest.previousPrice['foo-bar'] = 100
    underTest.previousPrice['biz-baz'] = 110
  })

  it('can iterate over an async iterator', async function(){

    const expected1 = {
      value: {
        bid: 10.01,
        ticker: 'foo-bar',
        previousPrice: 100,
        percent: '89.99'
      },
      done: false
    }
    const expected2 = {
      value: {
        bid: 10.01,
        ticker: 'biz-baz',
        previousPrice: 110,
        percent: '90.90'
      },
      done: false
    }

    const itr = underTest.run()
    const actual1 = await itr.next()
    const actual2 = await itr.next()
    assert.deepStrictEqual(actual1, expected1)
    assert.deepStrictEqual(actual2, expected2)
  })

  it('will throw error', async function(){
    underTest.client = {getTicker: ()=>{throw "error from client.getTicker"}}

    const itr = underTest.run()
    let actual
    try{
      actual = await itr.next()
    }catch(e){
      if(e=="error from client.getTicker"){
        return Promise.resolve(true)
      }
    }
    throw "error not found"
  })

  it('will diff two numbers', function(){
    const less = 1
    const more = 2

    const actual1 = Poller.diff(less, more)
    const actual2 = Poller.diff(more, less)

    assert.strictEqual(actual1, 1)
    assert.strictEqual(actual2, 1)
  })
})