#!/usr/env node

import assert from 'assert'
import Alert from '../../lib/Alert.mjs'
import {stdout} from 'test-console'

describe('Alert test', function(){

  it('will do alert', function(){
    const actual = stdout.inspectSync(function(){
      Alert(3,'down',5, 'foo-bar')
    })
    const expected = [ 'foo-bar ALERT!!!\n\tprice now 3\n\tprice down 5%\n' ]
    assert.deepStrictEqual(actual, expected)
  })

})