import Uphold from '../../lib/Uphold.mjs'
import nock from 'nock'
import assert, { doesNotMatch } from 'assert'
import sinon from 'sinon'

const config = {
  endpoint: 'https://api.uphold.com'
}
const underTest = new Uphold(config)

describe('Uphold library', function(){

  beforeEach(() => {
    if (!nock.isActive()) { // @see // @see https://github.com/nock/nock/issues/2057#issuecomment-702401375
      nock.activate()
    }
  })
  
  afterEach(() => {
    nock.restore() // @see https://github.com/nock/nock/issues/2057#issuecomment-702401375
  });

  it('will request a pair', async function(){

    const fixture = await import('../fixtures/uphold.ticker.pair.js')

    nock('https://api.uphold.com')
      .get('/v0/ticker/BTC-USD')
      .reply(200, fixture)

    const data = await underTest.getTicker('btc-usd')
    const actual = data.default
    const expected = { ask: '40790.6522', bid: '40643.55813', currency: 'USD' }
    assert.deepStrictEqual(actual, expected)
    nock.restore()
  })

  it('will handle error', function(done){

    let log = []
    sinon.stub(console, 'error').callsFake(function(...msg) {
      log = msg
    })

    const expectedStdout = [
      'error from https://api.uphold.com/v0/ticker/BTC-USD:\n',
      '{"msg":"this is the error"}',
    ]
    const expected = {"msg":"this is the error"}
    nock('https://api.uphold.com')
      .get('/v0/ticker/BTC-USD')
      .reply(500, expected)

    underTest.getTicker('btc-usd')
      .then(res => {
        done('promise resolved, error not thrown. Result: ', res)
      })
      .catch(e => {
        assert.deepStrictEqual(JSON.parse(e), expected)
        assert.deepStrictEqual(log, expectedStdout)
        console.error.restore()
        nock.restore()
        done()
      })
  })

})