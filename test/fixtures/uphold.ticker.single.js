export default [
    {
        "ask": "0.00318111",
        "bid": "0.00310546",
        "currency": "BTC",
        "pair": "AAPL-BTC"
    },
    {
        "ask": "0.01075775",
        "bid": "0.01059758",
        "currency": "BTC",
        "pair": "AAVE-BTC"
    },
    {
        "ask": "0.0000412",
        "bid": "0.00004067",
        "currency": "BTC",
        "pair": "ADABTC"
    },
    {
        "ask": "0.01231533",
        "bid": "0.01190286",
        "currency": "BTC",
        "pair": "ADBE-BTC"
    },
    {
        "ask": "0.00000672",
        "bid": "0.00000667",
        "currency": "BTC",
        "pair": "AEDBTC"
    },
    {
        "ask": "0.00196615",
        "bid": "0.00190029",
        "currency": "BTC",
        "pair": "AMD-BTC"
    },
    {
        "ask": "0.08077578",
        "bid": "0.07922542",
        "currency": "BTC",
        "pair": "AMZN-BTC"
    },
    {
        "ask": "2.7e-7",
        "bid": "2.6e-7",
        "currency": "BTC",
        "pair": "ARSBTC"
    },
    {
        "ask": "0.00038766",
        "bid": "0.00038249",
        "currency": "BTC",
        "pair": "ATOMBTC"
    },
    {
        "ask": "0.0000192",
        "bid": "0.00001907",
        "currency": "BTC",
        "pair": "AUDBTC"
    },
    {
        "ask": "0.00577365",
        "bid": "0.00563635",
        "currency": "BTC",
        "pair": "BA-BTC"
    },
    {
        "ask": "0.0053855",
        "bid": "0.00525382",
        "currency": "BTC",
        "pair": "BABA-BTC"
    },
    {
        "ask": "0.00104568",
        "bid": "0.00102081",
        "currency": "BTC",
        "pair": "BAC-BTC"
    },
    {
        "ask": "0.00090911",
        "bid": "0.00089296",
        "currency": "BTC",
        "pair": "BAL-BTC"
    },
    {
        "ask": "0.0000212",
        "bid": "0.00002094",
        "currency": "BTC",
        "pair": "BATBTC"
    },
    {
        "ask": "0.0194538",
        "bid": "0.01925072",
        "currency": "BTC",
        "pair": "BCHBTC"
    },
    {
        "ask": "0.00167816",
        "bid": "0.00162195",
        "currency": "BTC",
        "pair": "BMY-BTC"
    },
    {
        "ask": "0.00716739",
        "bid": "0.00699695",
        "currency": "BTC",
        "pair": "BRK.B-BTC"
    },
    {
        "ask": "0.00000467",
        "bid": "0.00000463",
        "currency": "BTC",
        "pair": "BRLBTC"
    },
    {
        "ask": "321.91970019",
        "bid": "314.4286557",
        "currency": "AAPL",
        "pair": "BTC-AAPL"
    },
    {
        "ask": "94.333680517115046149",
        "bid": "92.977742288990962496",
        "currency": "AAVE",
        "pair": "BTC-AAVE"
    },
    {
        "ask": "24580.38444635",
        "bid": "24280.62983783",
        "currency": "ADA",
        "pair": "BTCADA"
    },
    {
        "ask": "83.98898469",
        "bid": "81.2183134",
        "currency": "ADBE",
        "pair": "BTC-ADBE"
    },
    {
        "ask": "149727.47937",
        "bid": "148891.11135",
        "currency": "AED",
        "pair": "BTCAED"
    },
    {
        "ask": "526.08020871",
        "bid": "508.72536391",
        "currency": "AMD",
        "pair": "BTC-AMD"
    },
    {
        "ask": "12.61872874",
        "bid": "12.38256994",
        "currency": "AMZN",
        "pair": "BTC-AMZN"
    },
    {
        "ask": "3841636.11663",
        "bid": "3813656.24823",
        "currency": "ARS",
        "pair": "BTCARS"
    },
    {
        "ask": "2613.68167591",
        "bid": "2580.21740303",
        "currency": "ATOM",
        "pair": "BTCATOM"
    },
    {
        "ask": "52407.14251",
        "bid": "52115.054",
        "currency": "AUD",
        "pair": "BTCAUD"
    },
    {
        "ask": "177.36831031",
        "bid": "173.24073964",
        "currency": "BA",
        "pair": "BTC-BA"
    },
    {
        "ask": "190.28226875",
        "bid": "185.72637594",
        "currency": "BABA",
        "pair": "BTC-BABA"
    },
    {
        "ask": "979.32460045",
        "bid": "956.53689133",
        "currency": "BAC",
        "pair": "BTC-BAC"
    },
    {
        "ask": "1119.539630866495730142",
        "bid": "1100.239138144772644075",
        "currency": "BAL",
        "pair": "BTC-BAL"
    },
    {
        "ask": "47729.455630179214004416",
        "bid": "47188.398092801488508929",
        "currency": "BAT",
        "pair": "BTCBAT"
    },
    {
        "ask": "51.93111005",
        "bid": "51.41550129",
        "currency": "BCH",
        "pair": "BTCBCH"
    },
    {
        "ask": "616.36186202",
        "bid": "596.02864976",
        "currency": "BMY",
        "pair": "BTC-BMY"
    },
    {
        "ask": "142.87774283",
        "bid": "139.55305649",
        "currency": "BRK.B",
        "pair": "BTC-BRK.B"
    },
    {
        "ask": "215457.01536",
        "bid": "214252.22031",
        "currency": "BRL",
        "pair": "BTCBRL"
    },
    {
        "ask": "1.001850679623801113",
        "bid": "0.992797774683551931",
        "currency": "BTC0",
        "pair": "BTC-BTC0"
    },
    {
        "ask": "643.81374594",
        "bid": "616.00688514",
        "currency": "BTG",
        "pair": "BTCBTG"
    },
    {
        "ask": "537.87962409",
        "bid": "525.36327568",
        "currency": "C",
        "pair": "BTC-C"
    },
    {
        "ask": "49047.22477",
        "bid": "48773.91883",
        "currency": "CAD",
        "pair": "BTCCAD"
    },
    {
        "ask": "36609.706",
        "bid": "36405.55094",
        "currency": "CHF",
        "pair": "BTCCHF"
    },
    {
        "ask": "745.54379668",
        "bid": "728.19541969",
        "currency": "CMCSA",
        "pair": "BTC-CMCSA"
    },
    {
        "ask": "262076.02664",
        "bid": "260595.15289",
        "currency": "CNY",
        "pair": "BTCCNY"
    },
    {
        "ask": "77.29051388661942175",
        "bid": "76.308032104535031494",
        "currency": "COMP",
        "pair": "BTC-COMP"
    },
    {
        "ask": "789.60876554",
        "bid": "755.96206271",
        "currency": "CSCO",
        "pair": "BTC-CSCO"
    },
    {
        "ask": "850310.0305",
        "bid": "845360.35474",
        "currency": "CZK",
        "pair": "BTCCZK"
    },
    {
        "ask": "40853.295805451608724074",
        "bid": "40496.310898203592778977",
        "currency": "DAI",
        "pair": "BTC-DAI"
    },
    {
        "ask": "190.59297258",
        "bid": "187.89847899",
        "currency": "DASH",
        "pair": "BTCDASH"
    },
    {
        "ask": "289.610168",
        "bid": "279.618198",
        "currency": "DCR",
        "pair": "BTCDCR"
    },
    {
        "ask": "472240.979476141",
        "bid": "458189.9674771382",
        "currency": "DGB",
        "pair": "BTCDGB"
    },
    {
        "ask": "160.75237487",
        "bid": "157.01144133",
        "currency": "DHR",
        "pair": "BTC-DHR"
    },
    {
        "ask": "238.9153575",
        "bid": "233.35560904",
        "currency": "DIS",
        "pair": "BTC-DIS"
    },
    {
        "ask": "248376.96077",
        "bid": "246985.52529",
        "currency": "DKK",
        "pair": "BTCDKK"
    },
    {
        "ask": "105501.164983604",
        "bid": "104276.9858892117",
        "currency": "DOGE",
        "pair": "BTCDOGE"
    },
    {
        "ask": "1482.714839477367496907",
        "bid": "1462.348985495546873824",
        "currency": "DOT",
        "pair": "BTC-DOT"
    },
    {
        "ask": "767.70760743",
        "bid": "745.35759021",
        "currency": "EEM",
        "pair": "BTC-EEM"
    },
    {
        "ask": "514.87084554",
        "bid": "499.88154061",
        "currency": "EFA",
        "pair": "BTC-EFA"
    },
    {
        "ask": "6518.58531632",
        "bid": "6448.85670908",
        "currency": "EOS",
        "pair": "BTCEOS"
    },
    {
        "ask": "15.396658551044418539",
        "bid": "15.249390084391337766",
        "currency": "ETH",
        "pair": "BTCETH"
    },
    {
        "ask": "33402.9005",
        "bid": "33216.98643",
        "currency": "EUR",
        "pair": "BTCEUR"
    },
    {
        "ask": "1089.73603359",
        "bid": "1069.71388899",
        "currency": "EWZ",
        "pair": "BTC-EWZ"
    },
    {
        "ask": "129.04470375",
        "bid": "126.04162596",
        "currency": "FB",
        "pair": "BTC-FB"
    },
    {
        "ask": "477.136959111223406836",
        "bid": "472.101667067091616356",
        "currency": "FIL",
        "pair": "BTC-FIL"
    },
    {
        "ask": "905.24637541",
        "bid": "878.89222105",
        "currency": "FXI",
        "pair": "BTC-FXI"
    },
    {
        "ask": "28694.292",
        "bid": "28534.3656",
        "currency": "GBP",
        "pair": "BTCGBP"
    },
    {
        "ask": "1033.84232999",
        "bid": "1009.41074091",
        "currency": "GDX",
        "pair": "BTC-GDX"
    },
    {
        "ask": "232.29976866",
        "bid": "227.96572581",
        "currency": "GLD",
        "pair": "BTC-GLD"
    },
    {
        "ask": "17.45805274",
        "bid": "17.05139448",
        "currency": "GOOG",
        "pair": "BTC-GOOG"
    },
    {
        "ask": "17.83839006",
        "bid": "17.4182133",
        "currency": "GOOGL",
        "pair": "BTC-GOOGL"
    },
    {
        "ask": "47730.015073374279170494",
        "bid": "47038.513771677640716115",
        "currency": "GRT",
        "pair": "BTC-GRT"
    },
    {
        "ask": "157013.097133",
        "bid": "154733.431512",
        "currency": "HBAR",
        "pair": "BTC-HBAR"
    },
    {
        "ask": "129.26785669",
        "bid": "126.25952608",
        "currency": "HD",
        "pair": "BTC-HD"
    },
    {
        "ask": "316503.76202",
        "bid": "314723.24692",
        "currency": "HKD",
        "pair": "BTCHKD"
    },
    {
        "ask": "2669.95414675085404119",
        "bid": "2612.678677116358838602",
        "currency": "HNT",
        "pair": "BTC-HNT"
    },
    {
        "ask": "250932.63204",
        "bid": "249522.01253",
        "currency": "HRK",
        "pair": "BTCHRK"
    },
    {
        "ask": "11668005.98795",
        "bid": "11593515.05955",
        "currency": "HUF",
        "pair": "BTCHUF"
    },
    {
        "ask": "471.43013482",
        "bid": "462.76832149",
        "currency": "HYG",
        "pair": "BTC-HYG"
    },
    {
        "ask": "132578.09439",
        "bid": "131838.49954",
        "currency": "ILS",
        "pair": "BTCILS"
    },
    {
        "ask": "2968027.77887",
        "bid": "2948931.5927",
        "currency": "INR",
        "pair": "BTCINR"
    },
    {
        "ask": "731.87975115",
        "bid": "714.84954456",
        "currency": "INTC",
        "pair": "BTC-INTC"
    },
    {
        "ask": "30961.14757416",
        "bid": "30512.23282346",
        "currency": "IOTA",
        "pair": "BTCIOTA"
    },
    {
        "ask": "98.42225632",
        "bid": "96.13168976",
        "currency": "IVV",
        "pair": "BTC-IVV"
    },
    {
        "ask": "186.39460246",
        "bid": "182.05697038",
        "currency": "IWM",
        "pair": "BTC-IWM"
    },
    {
        "ask": "240.32513028",
        "bid": "234.73280272",
        "currency": "JNJ",
        "pair": "BTC-JNJ"
    },
    {
        "ask": "255.55572574",
        "bid": "249.5435185",
        "currency": "JPM",
        "pair": "BTC-JPM"
    },
    {
        "ask": "4435876.11088",
        "bid": "4405787.44799",
        "currency": "JPY",
        "pair": "BTCJPY"
    },
    {
        "ask": "4411846.44834",
        "bid": "4386735.18667",
        "currency": "KES",
        "pair": "BTCKES"
    },
    {
        "ask": "10335365.395939086294432367",
        "bid": "10019087.288888888888851818",
        "currency": "LBA",
        "pair": "BTCLBA"
    },
    {
        "ask": "1410.23130132",
        "bid": "1394.13308259",
        "currency": "LINK",
        "pair": "BTCLINK"
    },
    {
        "ask": "312.81318701",
        "bid": "307.0654982",
        "currency": "LQD",
        "pair": "BTC-LQD"
    },
    {
        "ask": "201.85160857",
        "bid": "199.55146902",
        "currency": "LTC",
        "pair": "BTCLTC"
    },
    {
        "ask": "111.85907677",
        "bid": "109.25601281",
        "currency": "MA",
        "pair": "BTC-MA"
    },
    {
        "ask": "10.761752475129590807",
        "bid": "10.610501749294578828",
        "currency": "MKR",
        "pair": "BTC-MKR"
    },
    {
        "ask": "166.80193709",
        "bid": "162.92030827",
        "currency": "MSFT",
        "pair": "BTC-MSFT"
    },
    {
        "ask": "505.61325619",
        "bid": "493.84810135",
        "currency": "MU",
        "pair": "BTC-MU"
    },
    {
        "ask": "810859.19664",
        "bid": "806224.76281",
        "currency": "MXN",
        "pair": "BTCMXN"
    },
    {
        "ask": "4861.680021",
        "bid": "4793.437442",
        "currency": "NANO",
        "pair": "BTCNANO"
    },
    {
        "ask": "636.759589",
        "bid": "625.296247",
        "currency": "NEO",
        "pair": "BTCNEO"
    },
    {
        "ask": "82.45297576",
        "bid": "79.73277832",
        "currency": "NFLX",
        "pair": "BTC-NFLX"
    },
    {
        "ask": "338779.96367",
        "bid": "336852.89094",
        "currency": "NOK",
        "pair": "BTCNOK"
    },
    {
        "ask": "70.29928473",
        "bid": "67.97996813",
        "currency": "NVDA",
        "pair": "BTC-NVDA"
    },
    {
        "ask": "56500.45157",
        "bid": "56185.36331",
        "currency": "NZD",
        "pair": "BTCNZD"
    },
    {
        "ask": "6421.39488053",
        "bid": "6331.30994161",
        "currency": "OMG",
        "pair": "BTCOMG"
    },
    {
        "ask": "82542.14083114",
        "bid": "81511.62766233",
        "currency": "OXT",
        "pair": "BTC-OXT"
    },
    {
        "ask": "297.94745475",
        "bid": "291.01433424",
        "currency": "PG",
        "pair": "BTC-PG"
    },
    {
        "ask": "1953062.2331",
        "bid": "1940568.96354",
        "currency": "PHP",
        "pair": "BTCPHP"
    },
    {
        "ask": "149925.7923",
        "bid": "149087.91127",
        "currency": "PLN",
        "pair": "BTCPLN"
    },
    {
        "ask": "124.79665359",
        "bid": "121.8921909",
        "currency": "QQQ",
        "pair": "BTC-QQQ"
    },
    {
        "ask": "78488.376816622335315676",
        "bid": "76973.411336216708375394",
        "currency": "REN",
        "pair": "BTC-REN"
    },
    {
        "ask": "122.36762568",
        "bid": "118.33072097",
        "currency": "ROKU",
        "pair": "BTC-ROKU"
    },
    {
        "ask": "164644.1133",
        "bid": "163723.73888",
        "currency": "RON",
        "pair": "BTCRON"
    },
    {
        "ask": "338020.91789",
        "bid": "336127.36875",
        "currency": "SEK",
        "pair": "BTCSEK"
    },
    {
        "ask": "54200.51031",
        "bid": "53898.83226",
        "currency": "SGD",
        "pair": "BTCSGD"
    },
    {
        "ask": "2259.675424285606484557",
        "bid": "2216.901666938926249046",
        "currency": "SNX",
        "pair": "BTC-SNX"
    },
    {
        "ask": "1014.464605019541265246",
        "bid": "1000.070328001981526249",
        "currency": "SOL",
        "pair": "BTC-SOL"
    },
    {
        "ask": "98.79811429",
        "bid": "96.44332346",
        "currency": "SPY",
        "pair": "BTC-SPY"
    },
    {
        "ask": "1415901.93558681",
        "bid": "1343619.32179518",
        "currency": "STORM",
        "pair": "BTCSTORM"
    },
    {
        "ask": "1377.97877867",
        "bid": "1332.52212802",
        "currency": "T",
        "pair": "BTC-T"
    },
    {
        "ask": "298.58515093",
        "bid": "293.09879033",
        "currency": "TLT",
        "pair": "BTC-TLT"
    },
    {
        "ask": "420.1986174",
        "bid": "402.29271987",
        "currency": "TQQQ",
        "pair": "BTC-TQQQ"
    },
    {
        "ask": "464113.7412816852",
        "bid": "458604.2441192968",
        "currency": "TRX",
        "pair": "BTCTRX"
    },
    {
        "ask": "69.42866249",
        "bid": "67.1181062",
        "currency": "TSLA",
        "pair": "BTC-TSLA"
    },
    {
        "ask": "40725.412201220122052516",
        "bid": "40573.246195380461913633",
        "currency": "TUSD",
        "pair": "BTC-TUSD"
    },
    {
        "ask": "2396.984540782420214806",
        "bid": "2360.515201262127653291",
        "currency": "UMA",
        "pair": "BTC-UMA"
    },
    {
        "ask": "100.61347161",
        "bid": "98.27173675",
        "currency": "UNH",
        "pair": "BTC-UNH"
    },
    {
        "ask": "1615.378102270856540734",
        "bid": "1597.03303338760771916",
        "currency": "UNI",
        "pair": "BTC-UNI"
    },
    {
        "ask": "1",
        "bid": "1",
        "currency": "UPBTC",
        "pair": "BTCUPBTC"
    },
    {
        "ask": "4596.285129610525081951",
        "bid": "4359.831818357637384812",
        "currency": "UPCO2",
        "pair": "BTC-UPCO2"
    },
    {
        "ask": "33402.9005",
        "bid": "33216.98643",
        "currency": "UPEUR",
        "pair": "BTCUPEUR"
    },
    {
        "ask": "2718380.48495936",
        "bid": "2490933.30366314",
        "currency": "UPT",
        "pair": "BTCUPT"
    },
    {
        "ask": "40762.46822",
        "bid": "40536.72621",
        "currency": "UPUSD",
        "pair": "BTCUPUSD"
    },
    {
        "ask": "21.82745249",
        "bid": "21.25073962",
        "currency": "UPXAU",
        "pair": "BTC-UPXAU"
    },
    {
        "ask": "40721.33966",
        "bid": "40577.30352",
        "currency": "USD",
        "pair": "BTCUSD"
    },
    {
        "ask": "40725.452516",
        "bid": "40573.245789",
        "currency": "USDC",
        "pair": "BTC-USDC"
    },
    {
        "ask": "40767.436217",
        "bid": "40439.381265",
        "currency": "USDT",
        "pair": "BTC-USDT"
    },
    {
        "ask": "181.54876304",
        "bid": "177.3240337",
        "currency": "V",
        "pair": "BTC-V"
    },
    {
        "ask": "10205849.53900838",
        "bid": "10119028.30908526",
        "currency": "VCO",
        "pair": "BTC-VCO"
    },
    {
        "ask": "4072133966",
        "bid": "12760158.33941847",
        "currency": "VOX",
        "pair": "BTCVOX"
    },
    {
        "ask": "1.00826037",
        "bid": "0.99292661",
        "currency": "WBTC",
        "pair": "BTC-WBTC"
    },
    {
        "ask": "898.46301465",
        "bid": "877.55641622",
        "currency": "WFC",
        "pair": "BTC-WFC"
    },
    {
        "ask": "1480.97241258",
        "bid": "1349.0318155",
        "currency": "XAG",
        "pair": "BTCXAG"
    },
    {
        "ask": "21.72931406",
        "bid": "21.20813345",
        "currency": "XAU",
        "pair": "BTCXAU"
    },
    {
        "ask": "185130.65889901",
        "bid": "179943.69630129",
        "currency": "XEM",
        "pair": "BTCXEM"
    },
    {
        "ask": "1102.13405266",
        "bid": "1076.48826981",
        "currency": "XLF",
        "pair": "BTC-XLF"
    },
    {
        "ask": "85814.04712787",
        "bid": "84700.15524759",
        "currency": "XLM",
        "pair": "BTCXLM"
    },
    {
        "ask": "624.6262579",
        "bid": "610.09152584",
        "currency": "XLU",
        "pair": "BTC-XLU"
    },
    {
        "ask": "693.61960926",
        "bid": "677.47947111",
        "currency": "XOM",
        "pair": "BTC-XOM"
    },
    {
        "ask": "14.52570908",
        "bid": "13.87175698",
        "currency": "XPD",
        "pair": "BTCXPD"
    },
    {
        "ask": "33.6126154",
        "bid": "32.19606147",
        "currency": "XPT",
        "pair": "BTCXPT"
    },
    {
        "ask": "36667.408133",
        "bid": "36325.41077",
        "currency": "XRP",
        "pair": "BTCXRP"
    },
    {
        "ask": "10349.735688",
        "bid": "10214.970965",
        "currency": "XTZ",
        "pair": "BTC-XTZ"
    },
    {
        "ask": "309785.7714761117",
        "bid": "305023.7053256498",
        "currency": "ZIL",
        "pair": "BTCZIL"
    },
    {
        "ask": "35348.07867083",
        "bid": "34898.0022703",
        "currency": "ZRX",
        "pair": "BTCZRX"
    },
    {
        "ask": "1.00748668",
        "bid": "0.99786216",
        "currency": "BTC",
        "pair": "BTC0-BTC"
    },
    {
        "ask": "0.00162374",
        "bid": "0.00155279",
        "currency": "BTC",
        "pair": "BTGBTC"
    },
    {
        "ask": "0.00190389",
        "bid": "0.00185861",
        "currency": "BTC",
        "pair": "C-BTC"
    },
    {
        "ask": "0.00002051",
        "bid": "0.00002038",
        "currency": "BTC",
        "pair": "CADBTC"
    },
    {
        "ask": "0.00002748",
        "bid": "0.0000273",
        "currency": "BTC",
        "pair": "CHFBTC"
    },
    {
        "ask": "0.00137358",
        "bid": "0.00134091",
        "currency": "BTC",
        "pair": "CMCSA-BTC"
    },
    {
        "ask": "0.00000384",
        "bid": "0.00000381",
        "currency": "BTC",
        "pair": "CNYBTC"
    },
    {
        "ask": "0.01310781",
        "bid": "0.01293443",
        "currency": "BTC",
        "pair": "COMP-BTC"
    },
    {
        "ask": "0.00132313",
        "bid": "0.00126608",
        "currency": "BTC",
        "pair": "CSCO-BTC"
    },
    {
        "ask": "0.00000119",
        "bid": "0.00000117",
        "currency": "BTC",
        "pair": "CZKBTC"
    },
    {
        "ask": "0.0000247",
        "bid": "0.00002447",
        "currency": "BTC",
        "pair": "DAI-BTC"
    },
    {
        "ask": "0.00532324",
        "bid": "0.00524526",
        "currency": "BTC",
        "pair": "DASHBTC"
    },
    {
        "ask": "0.00357706",
        "bid": "0.00345229",
        "currency": "BTC",
        "pair": "DCRBTC"
    },
    {
        "ask": "0.00000219",
        "bid": "0.00000211",
        "currency": "BTC",
        "pair": "DGBBTC"
    },
    {
        "ask": "0.00637043",
        "bid": "0.00621894",
        "currency": "BTC",
        "pair": "DHR-BTC"
    },
    {
        "ask": "0.00428629",
        "bid": "0.00418436",
        "currency": "BTC",
        "pair": "DIS-BTC"
    },
    {
        "ask": "0.00000405",
        "bid": "0.00000402",
        "currency": "BTC",
        "pair": "DKKBTC"
    },
    {
        "ask": "0.0000096",
        "bid": "0.00000947",
        "currency": "BTC",
        "pair": "DOGEBTC"
    },
    {
        "ask": "0.00068399",
        "bid": "0.00067424",
        "currency": "BTC",
        "pair": "DOT-BTC"
    },
    {
        "ask": "0.00134195",
        "bid": "0.0013022",
        "currency": "BTC",
        "pair": "EEM-BTC"
    },
    {
        "ask": "0.00200094",
        "bid": "0.00194166",
        "currency": "BTC",
        "pair": "EFA-BTC"
    },
    {
        "ask": "0.00015511",
        "bid": "0.00015336",
        "currency": "BTC",
        "pair": "EOSBTC"
    },
    {
        "ask": "0.06559152",
        "bid": "0.06493024",
        "currency": "BTC",
        "pair": "ETHBTC"
    },
    {
        "ask": "0.00003012",
        "bid": "0.00002992",
        "currency": "BTC",
        "pair": "EURBTC"
    },
    {
        "ask": "0.00093505",
        "bid": "0.00091738",
        "currency": "BTC",
        "pair": "EWZ-BTC"
    },
    {
        "ask": "0.00793571",
        "bid": "0.00774701",
        "currency": "BTC",
        "pair": "FB-BTC"
    },
    {
        "ask": "0.00211868",
        "bid": "0.00209522",
        "currency": "BTC",
        "pair": "FIL-BTC"
    },
    {
        "ask": "0.00113806",
        "bid": "0.00110435",
        "currency": "BTC",
        "pair": "FXI-BTC"
    },
    {
        "ask": "0.00003506",
        "bid": "0.00003484",
        "currency": "BTC",
        "pair": "GBPBTC"
    },
    {
        "ask": "0.00099091",
        "bid": "0.00096698",
        "currency": "BTC",
        "pair": "GDX-BTC"
    },
    {
        "ask": "0.00438764",
        "bid": "0.00430353",
        "currency": "BTC",
        "pair": "GLD-BTC"
    },
    {
        "ask": "0.05865935",
        "bid": "0.05726454",
        "currency": "BTC",
        "pair": "GOOG-BTC"
    },
    {
        "ask": "0.05742421",
        "bid": "0.0560426",
        "currency": "BTC",
        "pair": "GOOGL-BTC"
    },
    {
        "ask": "0.00002127",
        "bid": "0.00002094",
        "currency": "BTC",
        "pair": "GRT-BTC"
    },
    {
        "ask": "0.00000647",
        "bid": "0.00000636",
        "currency": "BTC",
        "pair": "HBAR-BTC"
    },
    {
        "ask": "0.00792202",
        "bid": "0.00773364",
        "currency": "BTC",
        "pair": "HD-BTC"
    },
    {
        "ask": "0.00000318",
        "bid": "0.00000315",
        "currency": "BTC",
        "pair": "HKDBTC"
    },
    {
        "ask": "0.00038284",
        "bid": "0.00037442",
        "currency": "BTC",
        "pair": "HNT-BTC"
    },
    {
        "ask": "0.00000401",
        "bid": "0.00000398",
        "currency": "BTC",
        "pair": "HRKBTC"
    },
    {
        "ask": "9e-8",
        "bid": "8e-8",
        "currency": "BTC",
        "pair": "HUFBTC"
    },
    {
        "ask": "0.00216141",
        "bid": "0.00212058",
        "currency": "BTC",
        "pair": "HYG-BTC"
    },
    {
        "ask": "0.00000759",
        "bid": "0.00000754",
        "currency": "BTC",
        "pair": "ILSBTC"
    },
    {
        "ask": "3.4e-7",
        "bid": "3.3e-7",
        "currency": "BTC",
        "pair": "INRBTC"
    },
    {
        "ask": "0.00139922",
        "bid": "0.00136594",
        "currency": "BTC",
        "pair": "INTC-BTC"
    },
    {
        "ask": "0.00003279",
        "bid": "0.00003228",
        "currency": "BTC",
        "pair": "IOTABTC"
    },
    {
        "ask": "0.01040476",
        "bid": "0.01015734",
        "currency": "BTC",
        "pair": "IVV-BTC"
    },
    {
        "ask": "0.00549405",
        "bid": "0.0053634",
        "currency": "BTC",
        "pair": "IWM-BTC"
    },
    {
        "ask": "0.00426115",
        "bid": "0.00415982",
        "currency": "BTC",
        "pair": "JNJ-BTC"
    },
    {
        "ask": "0.00400824",
        "bid": "0.0039119",
        "currency": "BTC",
        "pair": "JPM-BTC"
    },
    {
        "ask": "2.3e-7",
        "bid": "2.2e-7",
        "currency": "BTC",
        "pair": "JPYBTC"
    },
    {
        "ask": "2.3e-7",
        "bid": "2.2e-7",
        "currency": "BTC",
        "pair": "KESBTC"
    },
    {
        "ask": "1e-7",
        "bid": "9e-8",
        "currency": "BTC",
        "pair": "LBABTC"
    },
    {
        "ask": "0.00071746",
        "bid": "0.00070889",
        "currency": "BTC",
        "pair": "LINKBTC"
    },
    {
        "ask": "0.00325739",
        "bid": "0.00319586",
        "currency": "BTC",
        "pair": "LQD-BTC"
    },
    {
        "ask": "0.0050124",
        "bid": "0.00495269",
        "currency": "BTC",
        "pair": "LTCBTC"
    },
    {
        "ask": "0.00915492",
        "bid": "0.00893723",
        "currency": "BTC",
        "pair": "MA-BTC"
    },
    {
        "ask": "0.09426798",
        "bid": "0.09289461",
        "currency": "BTC",
        "pair": "MKR-BTC"
    },
    {
        "ask": "0.00613939",
        "bid": "0.00599339",
        "currency": "BTC",
        "pair": "MSFT-BTC"
    },
    {
        "ask": "0.00202539",
        "bid": "0.00197722",
        "currency": "BTC",
        "pair": "MU-BTC"
    },
    {
        "ask": "0.00000125",
        "bid": "0.00000123",
        "currency": "BTC",
        "pair": "MXNBTC"
    },
    {
        "ask": "0.00020867",
        "bid": "0.00020563",
        "currency": "BTC",
        "pair": "NANOBTC"
    },
    {
        "ask": "0.00159958",
        "bid": "0.00157004",
        "currency": "BTC",
        "pair": "NEOBTC"
    },
    {
        "ask": "0.01254478",
        "bid": "0.01212461",
        "currency": "BTC",
        "pair": "NFLX-BTC"
    },
    {
        "ask": "0.00000297",
        "bid": "0.00000295",
        "currency": "BTC",
        "pair": "NOKBTC"
    },
    {
        "ask": "0.01471359",
        "bid": "0.01422079",
        "currency": "BTC",
        "pair": "NVDA-BTC"
    },
    {
        "ask": "0.00001781",
        "bid": "0.00001769",
        "currency": "BTC",
        "pair": "NZDBTC"
    },
    {
        "ask": "0.00015799",
        "bid": "0.00015568",
        "currency": "BTC",
        "pair": "OMGBTC"
    },
    {
        "ask": "0.00001228",
        "bid": "0.00001211",
        "currency": "BTC",
        "pair": "OXT-BTC"
    },
    {
        "ask": "0.00343705",
        "bid": "0.00335532",
        "currency": "BTC",
        "pair": "PG-BTC"
    },
    {
        "ask": "5.2e-7",
        "bid": "5.1e-7",
        "currency": "BTC",
        "pair": "PHPBTC"
    },
    {
        "ask": "0.00000671",
        "bid": "0.00000666",
        "currency": "BTC",
        "pair": "PLNBTC"
    },
    {
        "ask": "0.00820585",
        "bid": "0.00801072",
        "currency": "BTC",
        "pair": "QQQ-BTC"
    },
    {
        "ask": "0.000013",
        "bid": "0.00001273",
        "currency": "BTC",
        "pair": "REN-BTC"
    },
    {
        "ask": "0.00845284",
        "bid": "0.00816973",
        "currency": "BTC",
        "pair": "ROKU-BTC"
    },
    {
        "ask": "0.00000611",
        "bid": "0.00000607",
        "currency": "BTC",
        "pair": "RONBTC"
    },
    {
        "ask": "0.00000298",
        "bid": "0.00000295",
        "currency": "BTC",
        "pair": "SEKBTC"
    },
    {
        "ask": "0.00001856",
        "bid": "0.00001844",
        "currency": "BTC",
        "pair": "SGDBTC"
    },
    {
        "ask": "0.00045119",
        "bid": "0.00044241",
        "currency": "BTC",
        "pair": "SNX-BTC"
    },
    {
        "ask": "0.00100017",
        "bid": "0.00098545",
        "currency": "BTC",
        "pair": "SOL-BTC"
    },
    {
        "ask": "0.01037115",
        "bid": "0.0101187",
        "currency": "BTC",
        "pair": "SPY-BTC"
    },
    {
        "ask": "7.5e-7",
        "bid": "7e-7",
        "currency": "BTC",
        "pair": "STORMBTC"
    },
    {
        "ask": "0.00075063",
        "bid": "0.00072548",
        "currency": "BTC",
        "pair": "T-BTC"
    },
    {
        "ask": "0.00341261",
        "bid": "0.00334815",
        "currency": "BTC",
        "pair": "TLT-BTC"
    },
    {
        "ask": "0.00248633",
        "bid": "0.00237913",
        "currency": "BTC",
        "pair": "TQQQ-BTC"
    },
    {
        "ask": "0.00000219",
        "bid": "0.00000215",
        "currency": "BTC",
        "pair": "TRXBTC"
    },
    {
        "ask": "0.0149025",
        "bid": "0.01439914",
        "currency": "BTC",
        "pair": "TSLA-BTC"
    },
    {
        "ask": "0.00002466",
        "bid": "0.00002454",
        "currency": "BTC",
        "pair": "TUSD-BTC"
    },
    {
        "ask": "0.00042374",
        "bid": "0.00041706",
        "currency": "BTC",
        "pair": "UMA-BTC"
    },
    {
        "ask": "0.0101782",
        "bid": "0.00993617",
        "currency": "BTC",
        "pair": "UNH-BTC"
    },
    {
        "ask": "0.00062631",
        "bid": "0.00061886",
        "currency": "BTC",
        "pair": "UNI-BTC"
    },
    {
        "ask": "1",
        "bid": "1",
        "currency": "BTC",
        "pair": "UPBTCBTC"
    },
    {
        "ask": "0.00022942",
        "bid": "0.0002175",
        "currency": "BTC",
        "pair": "UPCO2-BTC"
    },
    {
        "ask": "0.00003012",
        "bid": "0.00002992",
        "currency": "BTC",
        "pair": "UPEURBTC"
    },
    {
        "ask": "4.1e-7",
        "bid": "3.6e-7",
        "currency": "BTC",
        "pair": "UPTBTC"
    },
    {
        "ask": "0.00002468",
        "bid": "0.00002452",
        "currency": "BTC",
        "pair": "UPUSDBTC"
    },
    {
        "ask": "0.04706794",
        "bid": "0.04580132",
        "currency": "BTC",
        "pair": "UPXAU-BTC"
    },
    {
        "ask": "0.00002465",
        "bid": "0.00002455",
        "currency": "BTC",
        "pair": "USDBTC"
    },
    {
        "ask": "0.00002466",
        "bid": "0.00002454",
        "currency": "BTC",
        "pair": "USDC-BTC"
    },
    {
        "ask": "0.00002474",
        "bid": "0.00002452",
        "currency": "BTC",
        "pair": "USDT-BTC"
    },
    {
        "ask": "0.0056407",
        "bid": "0.00550656",
        "currency": "BTC",
        "pair": "V-BTC"
    },
    {
        "ask": "1e-7",
        "bid": "9e-8",
        "currency": "BTC",
        "pair": "VCO-BTC"
    },
    {
        "ask": "8e-8",
        "bid": "0",
        "currency": "BTC",
        "pair": "VOXBTC"
    },
    {
        "ask": "1.00731794",
        "bid": "0.99153384",
        "currency": "BTC",
        "pair": "WBTC-BTC"
    },
    {
        "ask": "0.0011398",
        "bid": "0.00111268",
        "currency": "BTC",
        "pair": "WFC-BTC"
    },
    {
        "ask": "0.00074145",
        "bid": "0.00067503",
        "currency": "BTC",
        "pair": "XAGBTC"
    },
    {
        "ask": "0.04716217",
        "bid": "0.04600758",
        "currency": "BTC",
        "pair": "XAUBTC"
    },
    {
        "ask": "0.00000556",
        "bid": "0.0000054",
        "currency": "BTC",
        "pair": "XEMBTC"
    },
    {
        "ask": "0.00092917",
        "bid": "0.00090706",
        "currency": "BTC",
        "pair": "XLF-BTC"
    },
    {
        "ask": "0.00001181",
        "bid": "0.00001164",
        "currency": "BTC",
        "pair": "XLMBTC"
    },
    {
        "ask": "0.00163948",
        "bid": "0.00160049",
        "currency": "BTC",
        "pair": "XLU-BTC"
    },
    {
        "ask": "0.0014764",
        "bid": "0.00144129",
        "currency": "BTC",
        "pair": "XOM-BTC"
    },
    {
        "ask": "0.07210412",
        "bid": "0.06882461",
        "currency": "BTC",
        "pair": "XPDBTC"
    },
    {
        "ask": "0.03106679",
        "bid": "0.0297424",
        "currency": "BTC",
        "pair": "XPTBTC"
    },
    {
        "ask": "0.00002754",
        "bid": "0.00002726",
        "currency": "BTC",
        "pair": "XRPBTC"
    },
    {
        "ask": "0.00009792",
        "bid": "0.00009659",
        "currency": "BTC",
        "pair": "XTZ-BTC"
    },
    {
        "ask": "0.00000328",
        "bid": "0.00000322",
        "currency": "BTC",
        "pair": "ZILBTC"
    },
    {
        "ask": "0.00002867",
        "bid": "0.00002828",
        "currency": "BTC",
        "pair": "ZRXBTC"
    }
]