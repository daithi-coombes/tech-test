export default [
    {
        "ask": "129.0509",
        "bid": "126.49549",
        "currency": "USD",
        "pair": "AAPL-USD"
    },
    {
        "ask": "434.16732",
        "bid": "429.51368",
        "currency": "USD",
        "pair": "AAVE-USD"
    },
    {
        "ask": "1.6788",
        "bid": "1.66432",
        "currency": "USD",
        "pair": "ADAUSD"
    },
    {
        "ask": "499.60761",
        "bid": "484.84161",
        "currency": "USD",
        "pair": "ADBE-USD"
    },
    {
        "ask": "0.2724",
        "bid": "0.27211",
        "currency": "USD",
        "pair": "AEDUSD"
    },
    {
        "ask": "79.76263",
        "bid": "77.40522",
        "currency": "USD",
        "pair": "AMD-USD"
    },
    {
        "ask": "3275.821",
        "bid": "3226.03285",
        "currency": "USD",
        "pair": "AMZN-USD"
    },
    {
        "ask": "0.01063",
        "bid": "0.01061",
        "currency": "USD",
        "pair": "ARSUSD"
    },
    {
        "ask": "15.76548",
        "bid": "15.62492",
        "currency": "USD",
        "pair": "ATOMUSD"
    },
    {
        "ask": "0.77807",
        "bid": "0.77726",
        "currency": "USD",
        "pair": "AUDUSD"
    },
    {
        "ask": "233.85109",
        "bid": "229.22048",
        "currency": "USD",
        "pair": "BA-USD"
    },
    {
        "ask": "218.61992",
        "bid": "214.14363",
        "currency": "USD",
        "pair": "BABA-USD"
    },
    {
        "ask": "42.42105",
        "bid": "41.58105",
        "currency": "USD",
        "pair": "BAC-USD"
    },
    {
        "ask": "36.85511",
        "bid": "36.41381",
        "currency": "USD",
        "pair": "BAL-USD"
    },
    {
        "ask": "0.86095",
        "bid": "0.85449",
        "currency": "USD",
        "pair": "BATUSD"
    },
    {
        "ask": "790.34197",
        "bid": "786.14814",
        "currency": "USD",
        "pair": "BCHUSD"
    },
    {
        "ask": "68.02866",
        "bid": "66.01805",
        "currency": "USD",
        "pair": "BMY-USD"
    },
    {
        "ask": "290.766",
        "bid": "285.00839",
        "currency": "USD",
        "pair": "BRK.B-USD"
    },
    {
        "ask": "0.18924",
        "bid": "0.18904",
        "currency": "USD",
        "pair": "BRLUSD"
    },
    {
        "ask": "40776.24284",
        "bid": "40624.21669",
        "currency": "USD",
        "pair": "BTCUSD"
    },
    {
        "ask": "40926.62858",
        "bid": "40692.98298",
        "currency": "USD",
        "pair": "BTC0-USD"
    },
    {
        "ask": "65.98664",
        "bid": "63.33125",
        "currency": "USD",
        "pair": "BTGUSD"
    },
    {
        "ask": "77.22652",
        "bid": "75.69731",
        "currency": "USD",
        "pair": "C-USD"
    },
    {
        "ask": "0.83121",
        "bid": "0.83035",
        "currency": "USD",
        "pair": "CADUSD"
    },
    {
        "ask": "1.11411",
        "bid": "1.11296",
        "currency": "USD",
        "pair": "CHFUSD"
    },
    {
        "ask": "55.72308",
        "bid": "54.61967",
        "currency": "USD",
        "pair": "CMCSA-USD"
    },
    {
        "ask": "0.15561",
        "bid": "0.15544",
        "currency": "USD",
        "pair": "CNYUSD"
    },
    {
        "ask": "539.43316",
        "bid": "535.05066",
        "currency": "USD",
        "pair": "COMP-USD"
    },
    {
        "ask": "53.60495",
        "bid": "51.50294",
        "currency": "USD",
        "pair": "CSCO-USD"
    },
    {
        "ask": "0.04796",
        "bid": "0.0479",
        "currency": "USD",
        "pair": "CZKUSD"
    },
    {
        "ask": "1.00183",
        "bid": "0.99649",
        "currency": "USD",
        "pair": "DAI-USD"
    },
    {
        "ask": "216.25036",
        "bid": "214.13158",
        "currency": "USD",
        "pair": "DASHUSD"
    },
    {
        "ask": "143.90818",
        "bid": "141.97514",
        "currency": "USD",
        "pair": "DCRUSD"
    },
    {
        "ask": "0.08888",
        "bid": "0.08674",
        "currency": "USD",
        "pair": "DGBUSD"
    },
    {
        "ask": "258.4351",
        "bid": "253.31769",
        "currency": "USD",
        "pair": "DHR-USD"
    },
    {
        "ask": "173.88591",
        "bid": "170.4427",
        "currency": "USD",
        "pair": "DIS-USD"
    },
    {
        "ask": "0.16416",
        "bid": "0.16399",
        "currency": "USD",
        "pair": "DKKUSD"
    },
    {
        "ask": "0.38933",
        "bid": "0.38605",
        "currency": "USD",
        "pair": "DOGEUSD"
    },
    {
        "ask": "27.76744",
        "bid": "27.51245",
        "currency": "USD",
        "pair": "DOT-USD"
    },
    {
        "ask": "54.42991",
        "bid": "53.03292",
        "currency": "USD",
        "pair": "EEM-USD"
    },
    {
        "ask": "81.15354",
        "bid": "79.07067",
        "currency": "USD",
        "pair": "EFA-USD"
    },
    {
        "ask": "6.30319",
        "bid": "6.26177",
        "currency": "USD",
        "pair": "EOSUSD"
    },
    {
        "ask": "2666.16562",
        "bid": "2649.71105",
        "currency": "USD",
        "pair": "ETHUSD"
    },
    {
        "ask": "1.22058",
        "bid": "1.21932",
        "currency": "USD",
        "pair": "EURUSD"
    },
    {
        "ask": "37.93285",
        "bid": "37.36809",
        "currency": "USD",
        "pair": "EWZ-USD"
    },
    {
        "ask": "321.80407",
        "bid": "315.43186",
        "currency": "USD",
        "pair": "FB-USD"
    },
    {
        "ask": "85.98881",
        "bid": "85.44842",
        "currency": "USD",
        "pair": "FIL-USD"
    },
    {
        "ask": "46.16868",
        "bid": "44.98372",
        "currency": "USD",
        "pair": "FXI-USD"
    },
    {
        "ask": "1.42111",
        "bid": "1.41964",
        "currency": "USD",
        "pair": "GBPUSD"
    },
    {
        "ask": "40.1788",
        "bid": "39.36857",
        "currency": "USD",
        "pair": "GDX-USD"
    },
    {
        "ask": "178.03754",
        "bid": "175.33624",
        "currency": "USD",
        "pair": "GLD-USD"
    },
    {
        "ask": "2379.68961",
        "bid": "2332.5682",
        "currency": "USD",
        "pair": "GOOG-USD"
    },
    {
        "ask": "2329.58227",
        "bid": "2282.79448",
        "currency": "USD",
        "pair": "GOOGL-USD"
    },
    {
        "ask": "0.86393",
        "bid": "0.85583",
        "currency": "USD",
        "pair": "GRT-USD"
    },
    {
        "ask": "0.26403",
        "bid": "0.26051",
        "currency": "USD",
        "pair": "HBAR-USD"
    },
    {
        "ask": "321.37986",
        "bid": "315.01605",
        "currency": "USD",
        "pair": "HD-USD"
    },
    {
        "ask": "0.12886",
        "bid": "0.12872",
        "currency": "USD",
        "pair": "HKDUSD"
    },
    {
        "ask": "15.48975",
        "bid": "15.24977",
        "currency": "USD",
        "pair": "HNT-USD"
    },
    {
        "ask": "0.16254",
        "bid": "0.16237",
        "currency": "USD",
        "pair": "HRKUSD"
    },
    {
        "ask": "0.0035",
        "bid": "0.00349",
        "currency": "USD",
        "pair": "HUFUSD"
    },
    {
        "ask": "87.68382",
        "bid": "86.37836",
        "currency": "USD",
        "pair": "HYG-USD"
    },
    {
        "ask": "0.30775",
        "bid": "0.30742",
        "currency": "USD",
        "pair": "ILSUSD"
    },
    {
        "ask": "0.01375",
        "bid": "0.01372",
        "currency": "USD",
        "pair": "INRUSD"
    },
    {
        "ask": "56.80381",
        "bid": "55.679",
        "currency": "USD",
        "pair": "INTC-USD"
    },
    {
        "ask": "1.33395",
        "bid": "1.32078",
        "currency": "USD",
        "pair": "IOTAUSD"
    },
    {
        "ask": "422.09955",
        "bid": "413.74134",
        "currency": "USD",
        "pair": "IVV-USD"
    },
    {
        "ask": "222.80142",
        "bid": "218.38961",
        "currency": "USD",
        "pair": "IWM-USD"
    },
    {
        "ask": "172.75468",
        "bid": "169.33387",
        "currency": "USD",
        "pair": "JNJ-USD"
    },
    {
        "ask": "162.60604",
        "bid": "159.34452",
        "currency": "USD",
        "pair": "JPM-USD"
    },
    {
        "ask": "0.00921",
        "bid": "0.00919",
        "currency": "USD",
        "pair": "JPYUSD"
    },
    {
        "ask": "0.00925",
        "bid": "0.00923",
        "currency": "USD",
        "pair": "KESUSD"
    },
    {
        "ask": "0.00406",
        "bid": "0.00394",
        "currency": "USD",
        "pair": "LBAUSD"
    },
    {
        "ask": "29.1669",
        "bid": "28.96902",
        "currency": "USD",
        "pair": "LINKUSD"
    },
    {
        "ask": "132.16549",
        "bid": "130.19778",
        "currency": "USD",
        "pair": "LQD-USD"
    },
    {
        "ask": "203.90052",
        "bid": "202.76643",
        "currency": "USD",
        "pair": "LTCUSD"
    },
    {
        "ask": "371.4973",
        "bid": "364.14109",
        "currency": "USD",
        "pair": "MA-USD"
    },
    {
        "ask": "3841.67704",
        "bid": "3807.55462",
        "currency": "USD",
        "pair": "MKR-USD"
    },
    {
        "ask": "248.96107",
        "bid": "244.03126",
        "currency": "USD",
        "pair": "MSFT-USD"
    },
    {
        "ask": "82.11504",
        "bid": "80.48903",
        "currency": "USD",
        "pair": "MU-USD"
    },
    {
        "ask": "0.05027",
        "bid": "0.05021",
        "currency": "USD",
        "pair": "MXNUSD"
    },
    {
        "ask": "8.45325",
        "bid": "8.37094",
        "currency": "USD",
        "pair": "NANOUSD"
    },
    {
        "ask": "65.05751",
        "bid": "64.1595",
        "currency": "USD",
        "pair": "NEOUSD"
    },
    {
        "ask": "508.91562",
        "bid": "493.87451",
        "currency": "USD",
        "pair": "NFLX-USD"
    },
    {
        "ask": "0.12036",
        "bid": "0.12022",
        "currency": "USD",
        "pair": "NOKUSD"
    },
    {
        "ask": "596.89001",
        "bid": "579.2488",
        "currency": "USD",
        "pair": "NVDA-USD"
    },
    {
        "ask": "0.72163",
        "bid": "0.72088",
        "currency": "USD",
        "pair": "NZDUSD"
    },
    {
        "ask": "6.43883",
        "bid": "6.37803",
        "currency": "USD",
        "pair": "OMGUSD"
    },
    {
        "ask": "0.49842",
        "bid": "0.49344",
        "currency": "USD",
        "pair": "OXT-USD"
    },
    {
        "ask": "139.43396",
        "bid": "136.67295",
        "currency": "USD",
        "pair": "PG-USD"
    },
    {
        "ask": "0.0209",
        "bid": "0.02086",
        "currency": "USD",
        "pair": "PHPUSD"
    },
    {
        "ask": "0.27206",
        "bid": "0.27178",
        "currency": "USD",
        "pair": "PLNUSD"
    },
    {
        "ask": "332.83354",
        "bid": "326.24293",
        "currency": "USD",
        "pair": "QQQ-USD"
    },
    {
        "ask": "0.52978",
        "bid": "0.52188",
        "currency": "USD",
        "pair": "REN-USD"
    },
    {
        "ask": "342.10231",
        "bid": "331.9914",
        "currency": "USD",
        "pair": "ROKU-USD"
    },
    {
        "ask": "0.24775",
        "bid": "0.24749",
        "currency": "USD",
        "pair": "RONUSD"
    },
    {
        "ask": "0.12063",
        "bid": "0.12049",
        "currency": "USD",
        "pair": "SEKUSD"
    },
    {
        "ask": "0.75245",
        "bid": "0.75166",
        "currency": "USD",
        "pair": "SGDUSD"
    },
    {
        "ask": "18.0483",
        "bid": "17.93181",
        "currency": "USD",
        "pair": "SNX-USD"
    },
    {
        "ask": "40.73308",
        "bid": "40.39392",
        "currency": "USD",
        "pair": "SOL-USD"
    },
    {
        "ask": "420.73614",
        "bid": "412.1672",
        "currency": "USD",
        "pair": "SPY-USD"
    },
    {
        "ask": "0.03057",
        "bid": "0.0288",
        "currency": "USD",
        "pair": "STORMUSD"
    },
    {
        "ask": "30.4515",
        "bid": "29.5515",
        "currency": "USD",
        "pair": "T-USD"
    },
    {
        "ask": "138.50275",
        "bid": "136.44069",
        "currency": "USD",
        "pair": "TLT-USD"
    },
    {
        "ask": "100.82422",
        "bid": "96.87061",
        "currency": "USD",
        "pair": "TQQQ-USD"
    },
    {
        "ask": "0.08858",
        "bid": "0.08792",
        "currency": "USD",
        "pair": "TRXUSD"
    },
    {
        "ask": "604.15776",
        "bid": "586.12911",
        "currency": "USD",
        "pair": "TSLA-USD"
    },
    {
        "ask": "1.0001",
        "bid": "0.9999",
        "currency": "USD",
        "pair": "TUSD-USD"
    },
    {
        "ask": "17.28632",
        "bid": "17.10006",
        "currency": "USD",
        "pair": "UMA-USD"
    },
    {
        "ask": "412.90833",
        "bid": "404.73212",
        "currency": "USD",
        "pair": "UNH-USD"
    },
    {
        "ask": "25.44304",
        "bid": "25.24412",
        "currency": "USD",
        "pair": "UNI-USD"
    },
    {
        "ask": "40776.24284",
        "bid": "40624.21669",
        "currency": "USD",
        "pair": "UPBTCUSD"
    },
    {
        "ask": "9.30708",
        "bid": "8.85962",
        "currency": "USD",
        "pair": "UPCO2-USD"
    },
    {
        "ask": "1.22058",
        "bid": "1.21932",
        "currency": "USD",
        "pair": "UPEURUSD"
    },
    {
        "ask": "0.01633",
        "bid": "0.01501",
        "currency": "USD",
        "pair": "UPTUSD"
    },
    {
        "ask": "1.001",
        "bid": "0.999",
        "currency": "USD",
        "pair": "UPUSDUSD"
    },
    {
        "ask": "1909.34854",
        "bid": "1865.53372",
        "currency": "USD",
        "pair": "UPXAU-USD"
    },
    {
        "ask": "0.00790543",
        "bid": "0.00774888",
        "currency": "AAPL",
        "pair": "USD-AAPL"
    },
    {
        "ask": "0.00232821455186247",
        "bid": "0.002303259489912782",
        "currency": "AAVE",
        "pair": "USD-AAVE"
    },
    {
        "ask": "0.600846",
        "bid": "0.59566356",
        "currency": "ADA",
        "pair": "USDADA"
    },
    {
        "ask": "0.00206253",
        "bid": "0.00200157",
        "currency": "ADBE",
        "pair": "USD-ADBE"
    },
    {
        "ask": "3.67499",
        "bid": "3.67107",
        "currency": "AED",
        "pair": "USDAED"
    },
    {
        "ask": "0.01291903",
        "bid": "0.01253719",
        "currency": "AMD",
        "pair": "USD-AMD"
    },
    {
        "ask": "0.00030998",
        "bid": "0.00030526",
        "currency": "AMZN",
        "pair": "USD-AMZN"
    },
    {
        "ask": "94.25071",
        "bid": "94.07337",
        "currency": "ARS",
        "pair": "USDARS"
    },
    {
        "ask": "0.06400033",
        "bid": "0.06342972",
        "currency": "ATOM",
        "pair": "USDATOM"
    },
    {
        "ask": "1.28658",
        "bid": "1.28523",
        "currency": "AUD",
        "pair": "USDAUD"
    },
    {
        "ask": "0.00436262",
        "bid": "0.00427622",
        "currency": "BA",
        "pair": "USD-BA"
    },
    {
        "ask": "0.00466977",
        "bid": "0.00457414",
        "currency": "BABA",
        "pair": "USD-BABA"
    },
    {
        "ask": "0.02404942",
        "bid": "0.0235732",
        "currency": "BAC",
        "pair": "USD-BAC"
    },
    {
        "ask": "0.02746210846928679",
        "bid": "0.027133279482817986",
        "currency": "BAL",
        "pair": "USD-BAL"
    },
    {
        "ask": "1.170288710224812462",
        "bid": "1.161507636912712701",
        "currency": "BAT",
        "pair": "USDBAT"
    },
    {
        "ask": "0.00127203",
        "bid": "0.00126527",
        "currency": "BCH",
        "pair": "USDBCH"
    },
    {
        "ask": "0.01514738",
        "bid": "0.01469968",
        "currency": "BMY",
        "pair": "USD-BMY"
    },
    {
        "ask": "0.00350867",
        "bid": "0.00343919",
        "currency": "BRK.B",
        "pair": "USD-BRK.B"
    },
    {
        "ask": "5.28989",
        "bid": "5.28429",
        "currency": "BRL",
        "pair": "USDBRL"
    },
    {
        "ask": "0.00002462",
        "bid": "0.00002452",
        "currency": "BTC",
        "pair": "USDBTC"
    },
    {
        "ask": "0.000024574261377975",
        "bid": "0.000024433969635326",
        "currency": "BTC0",
        "pair": "USD-BTC0"
    },
    {
        "ask": "0.01579",
        "bid": "0.01515458",
        "currency": "BTG",
        "pair": "USDBTG"
    },
    {
        "ask": "0.01321051",
        "bid": "0.01294891",
        "currency": "C",
        "pair": "USD-C"
    },
    {
        "ask": "1.20432",
        "bid": "1.20306",
        "currency": "CAD",
        "pair": "USDCAD"
    },
    {
        "ask": "0.89851",
        "bid": "0.89757",
        "currency": "CHF",
        "pair": "USDCHF"
    },
    {
        "ask": "0.01830843",
        "bid": "0.01794588",
        "currency": "CMCSA",
        "pair": "USD-CMCSA"
    },
    {
        "ask": "6.43336",
        "bid": "6.42632",
        "currency": "CNY",
        "pair": "USDCNY"
    },
    {
        "ask": "0.001868981901638997",
        "bid": "0.001853797790258203",
        "currency": "COMP",
        "pair": "USD-COMP"
    },
    {
        "ask": "0.01941637",
        "bid": "0.01865499",
        "currency": "CSCO",
        "pair": "USD-CSCO"
    },
    {
        "ask": "20.87683",
        "bid": "20.8507",
        "currency": "CZK",
        "pair": "USDCZK"
    },
    {
        "ask": "1.003522363495870506",
        "bid": "0.998173342782707645",
        "currency": "DAI",
        "pair": "USD-DAI"
    },
    {
        "ask": "0.00467003",
        "bid": "0.00462426",
        "currency": "DASH",
        "pair": "USDDASH"
    },
    {
        "ask": "0.007044",
        "bid": "0.006948",
        "currency": "DCR",
        "pair": "USDDCR"
    },
    {
        "ask": "11.5287064792",
        "bid": "11.2511251125",
        "currency": "DGB",
        "pair": "USDDGB"
    },
    {
        "ask": "0.00394762",
        "bid": "0.00386944",
        "currency": "DHR",
        "pair": "USD-DHR"
    },
    {
        "ask": "0.00586708",
        "bid": "0.00575089",
        "currency": "DIS",
        "pair": "USD-DIS"
    },
    {
        "ask": "6.09794",
        "bid": "6.09161",
        "currency": "DKK",
        "pair": "USDDKK"
    },
    {
        "ask": "2.5903380392",
        "bid": "2.5685151413",
        "currency": "DOGE",
        "pair": "USDDOGE"
    },
    {
        "ask": "0.036347181003509321",
        "bid": "0.036013402747966683",
        "currency": "DOT",
        "pair": "USD-DOT"
    },
    {
        "ask": "0.01885622",
        "bid": "0.01837225",
        "currency": "EEM",
        "pair": "USD-EEM"
    },
    {
        "ask": "0.01264692",
        "bid": "0.01232232",
        "currency": "EFA",
        "pair": "USD-EFA"
    },
    {
        "ask": "0.15969926",
        "bid": "0.15864982",
        "currency": "EOS",
        "pair": "USDEOS"
    },
    {
        "ask": "0.000377399641368443",
        "bid": "0.000375070472928834",
        "currency": "ETH",
        "pair": "USDETH"
    },
    {
        "ask": "0.82013",
        "bid": "0.81928",
        "currency": "EUR",
        "pair": "USDEUR"
    },
    {
        "ask": "0.02676081",
        "bid": "0.02636237",
        "currency": "EWZ",
        "pair": "USD-EWZ"
    },
    {
        "ask": "0.00317026",
        "bid": "0.00310748",
        "currency": "FB",
        "pair": "USD-FB"
    },
    {
        "ask": "0.011702966538175897",
        "bid": "0.011629420153622314",
        "currency": "FIL",
        "pair": "USD-FIL"
    },
    {
        "ask": "0.02223027",
        "bid": "0.0216597",
        "currency": "FXI",
        "pair": "USD-FXI"
    },
    {
        "ask": "0.70441",
        "bid": "0.70367",
        "currency": "GBP",
        "pair": "USDGBP"
    },
    {
        "ask": "0.02540098",
        "bid": "0.02488874",
        "currency": "GDX",
        "pair": "USD-GDX"
    },
    {
        "ask": "0.00570333",
        "bid": "0.00561679",
        "currency": "GLD",
        "pair": "USD-GLD"
    },
    {
        "ask": "0.00042872",
        "bid": "0.00042022",
        "currency": "GOOG",
        "pair": "USD-GOOG"
    },
    {
        "ask": "0.00043806",
        "bid": "0.00042926",
        "currency": "GOOGL",
        "pair": "USD-GOOGL"
    },
    {
        "ask": "1.16845635231295935",
        "bid": "1.157501186438716099",
        "currency": "GRT",
        "pair": "USD-GRT"
    },
    {
        "ask": "3.838625",
        "bid": "3.787448",
        "currency": "HBAR",
        "pair": "USD-HBAR"
    },
    {
        "ask": "0.00317445",
        "bid": "0.00311158",
        "currency": "HD",
        "pair": "USD-HD"
    },
    {
        "ask": "7.76881",
        "bid": "7.76036",
        "currency": "HKD",
        "pair": "USDHKD"
    },
    {
        "ask": "0.065574759488175888",
        "bid": "0.064558821155925692",
        "currency": "HNT",
        "pair": "USD-HNT"
    },
    {
        "ask": "6.15878",
        "bid": "6.15233",
        "currency": "HRK",
        "pair": "USDHRK"
    },
    {
        "ask": "286.53296",
        "bid": "285.71428",
        "currency": "HUF",
        "pair": "USDHUF"
    },
    {
        "ask": "0.01157698",
        "bid": "0.01140461",
        "currency": "HYG",
        "pair": "USD-HYG"
    },
    {
        "ask": "3.25288",
        "bid": "3.24939",
        "currency": "ILS",
        "pair": "USDILS"
    },
    {
        "ask": "72.8863",
        "bid": "72.72727",
        "currency": "INR",
        "pair": "USDINR"
    },
    {
        "ask": "0.0179601",
        "bid": "0.01760445",
        "currency": "INTC",
        "pair": "USD-INTC"
    },
    {
        "ask": "0.75712837",
        "bid": "0.74965328",
        "currency": "IOTA",
        "pair": "USDIOTA"
    },
    {
        "ask": "0.00241697",
        "bid": "0.0023691",
        "currency": "IVV",
        "pair": "USD-IVV"
    },
    {
        "ask": "0.00457898",
        "bid": "0.0044883",
        "currency": "IWM",
        "pair": "USD-IWM"
    },
    {
        "ask": "0.0059055",
        "bid": "0.00578855",
        "currency": "JNJ",
        "pair": "USD-JNJ"
    },
    {
        "ask": "0.00627572",
        "bid": "0.00614983",
        "currency": "JPM",
        "pair": "USD-JPM"
    },
    {
        "ask": "108.81393",
        "bid": "108.57763",
        "currency": "JPY",
        "pair": "USDJPY"
    },
    {
        "ask": "108.34237",
        "bid": "108.1081",
        "currency": "KES",
        "pair": "USDKES"
    },
    {
        "ask": "253.807106598984771574",
        "bid": "246.30541871921182266",
        "currency": "LBA",
        "pair": "USDLBA"
    },
    {
        "ask": "0.03451964",
        "bid": "0.03428544",
        "currency": "LINK",
        "pair": "USDLINK"
    },
    {
        "ask": "0.00768063",
        "bid": "0.00756627",
        "currency": "LQD",
        "pair": "USD-LQD"
    },
    {
        "ask": "0.00493179",
        "bid": "0.00490435",
        "currency": "LTC",
        "pair": "USDLTC"
    },
    {
        "ask": "0.00274619",
        "bid": "0.0026918",
        "currency": "MA",
        "pair": "USD-MA"
    },
    {
        "ask": "0.000262635759641447",
        "bid": "0.000260302984761051",
        "currency": "MKR",
        "pair": "USD-MKR"
    },
    {
        "ask": "0.00409784",
        "bid": "0.00401669",
        "currency": "MSFT",
        "pair": "USD-MSFT"
    },
    {
        "ask": "0.01242406",
        "bid": "0.01217803",
        "currency": "MU",
        "pair": "USD-MU"
    },
    {
        "ask": "19.91636",
        "bid": "19.89258",
        "currency": "MXN",
        "pair": "USDMXN"
    },
    {
        "ask": "0.119461",
        "bid": "0.118297",
        "currency": "NANO",
        "pair": "USDNANO"
    },
    {
        "ask": "0.015587",
        "bid": "0.015371",
        "currency": "NEO",
        "pair": "USDNEO"
    },
    {
        "ask": "0.00202481",
        "bid": "0.00196496",
        "currency": "NFLX",
        "pair": "USD-NFLX"
    },
    {
        "ask": "8.31809",
        "bid": "8.3084",
        "currency": "NOK",
        "pair": "USDNOK"
    },
    {
        "ask": "0.00172638",
        "bid": "0.00167535",
        "currency": "NVDA",
        "pair": "USD-NVDA"
    },
    {
        "ask": "1.3872",
        "bid": "1.38575",
        "currency": "NZD",
        "pair": "USDNZD"
    },
    {
        "ask": "0.15678823",
        "bid": "0.15530771",
        "currency": "OMG",
        "pair": "USDOMG"
    },
    {
        "ask": "2.02658885",
        "bid": "2.00634003",
        "currency": "OXT",
        "pair": "USD-OXT"
    },
    {
        "ask": "0.00731674",
        "bid": "0.00717185",
        "currency": "PG",
        "pair": "USD-PG"
    },
    {
        "ask": "47.93864",
        "bid": "47.84688",
        "currency": "PHP",
        "pair": "USDPHP"
    },
    {
        "ask": "3.67945",
        "bid": "3.67565",
        "currency": "PLN",
        "pair": "USDPLN"
    },
    {
        "ask": "0.00306521",
        "bid": "0.0030045",
        "currency": "QQQ",
        "pair": "USD-QQQ"
    },
    {
        "ask": "1.9161493063539511",
        "bid": "1.887575974932991052",
        "currency": "REN",
        "pair": "USD-REN"
    },
    {
        "ask": "0.00301213",
        "bid": "0.0029231",
        "currency": "ROKU",
        "pair": "USD-ROKU"
    },
    {
        "ask": "4.04057",
        "bid": "4.03632",
        "currency": "RON",
        "pair": "USDRON"
    },
    {
        "ask": "8.29945",
        "bid": "8.28981",
        "currency": "SEK",
        "pair": "USDSEK"
    },
    {
        "ask": "1.33039",
        "bid": "1.32899",
        "currency": "SGD",
        "pair": "USDSGD"
    },
    {
        "ask": "0.055766818854315321",
        "bid": "0.055406880426411351",
        "currency": "SNX",
        "pair": "USD-SNX"
    },
    {
        "ask": "0.024756200933209751",
        "bid": "0.024550070851504477",
        "currency": "SOL",
        "pair": "USD-SOL"
    },
    {
        "ask": "0.0024262",
        "bid": "0.00237678",
        "currency": "SPY",
        "pair": "USD-SPY"
    },
    {
        "ask": "34.72222223",
        "bid": "32.71180896",
        "currency": "STORM",
        "pair": "USDSTORM"
    },
    {
        "ask": "0.03383923",
        "bid": "0.0328391",
        "currency": "T",
        "pair": "USD-T"
    },
    {
        "ask": "0.0073292",
        "bid": "0.00722007",
        "currency": "TLT",
        "pair": "USD-TLT"
    },
    {
        "ask": "0.01032305",
        "bid": "0.00991825",
        "currency": "TQQQ",
        "pair": "USD-TQQQ"
    },
    {
        "ask": "11.3739763422",
        "bid": "11.2892300745",
        "currency": "TRX",
        "pair": "USDTRX"
    },
    {
        "ask": "0.00170611",
        "bid": "0.00165519",
        "currency": "TSLA",
        "pair": "USD-TSLA"
    },
    {
        "ask": "1.000100010001000101",
        "bid": "0.999900009999000099",
        "currency": "TUSD",
        "pair": "USD-TUSD"
    },
    {
        "ask": "0.058479326973121732",
        "bid": "0.05784921255651868",
        "currency": "UMA",
        "pair": "USD-UMA"
    },
    {
        "ask": "0.00247078",
        "bid": "0.00242184",
        "currency": "UNH",
        "pair": "USD-UNH"
    },
    {
        "ask": "0.039613185169457284",
        "bid": "0.039303479458429495",
        "currency": "UNI",
        "pair": "USD-UNI"
    },
    {
        "ask": "0.00002462",
        "bid": "0.00002452",
        "currency": "UPBTC",
        "pair": "USDUPBTC"
    },
    {
        "ask": "0.112871658152381254",
        "bid": "0.107445084817149954",
        "currency": "UPCO2",
        "pair": "USD-UPCO2"
    },
    {
        "ask": "0.82013",
        "bid": "0.81928",
        "currency": "UPEUR",
        "pair": "USDUPEUR"
    },
    {
        "ask": "66.62225184",
        "bid": "61.23698714",
        "currency": "UPT",
        "pair": "USDUPT"
    },
    {
        "ask": "1.00101",
        "bid": "0.999",
        "currency": "UPUSD",
        "pair": "USDUPUSD"
    },
    {
        "ask": "0.00053604",
        "bid": "0.00052373",
        "currency": "UPXAU",
        "pair": "USD-UPXAU"
    },
    {
        "ask": "1.000101",
        "bid": "0.9999",
        "currency": "USDC",
        "pair": "USD-USDC"
    },
    {
        "ask": "1.001022",
        "bid": "0.9967",
        "currency": "USDT",
        "pair": "USD-USDT"
    },
    {
        "ask": "0.00445832",
        "bid": "0.00437003",
        "currency": "V",
        "pair": "USD-V"
    },
    {
        "ask": "250.62656642",
        "bid": "249.3765586",
        "currency": "VCO",
        "pair": "USD-VCO"
    },
    {
        "ask": "100000",
        "bid": "314.4654088",
        "currency": "VOX",
        "pair": "USDVOX"
    },
    {
        "ask": "0.0000247",
        "bid": "0.00002445",
        "currency": "WBTC",
        "pair": "USD-WBTC"
    },
    {
        "ask": "0.02208782",
        "bid": "0.02165043",
        "currency": "WFC",
        "pair": "USD-WFC"
    },
    {
        "ask": "0.036377",
        "bid": "0.03323551",
        "currency": "XAG",
        "pair": "USDXAG"
    },
    {
        "ask": "0.0005341",
        "bid": "0.00052251",
        "currency": "XAU",
        "pair": "USDXAU"
    },
    {
        "ask": "4.52365874",
        "bid": "4.43734469",
        "currency": "XEM",
        "pair": "USDXEM"
    },
    {
        "ask": "0.02705077",
        "bid": "0.02651511",
        "currency": "XLF",
        "pair": "USD-XLF"
    },
    {
        "ask": "2.10110519",
        "bid": "2.08229218",
        "currency": "XLM",
        "pair": "USDXLM"
    },
    {
        "ask": "0.01533904",
        "bid": "0.01503529",
        "currency": "XLU",
        "pair": "USD-XLU"
    },
    {
        "ask": "0.01703332",
        "bid": "0.01669602",
        "currency": "XOM",
        "pair": "USD-XOM"
    },
    {
        "ask": "0.0003561",
        "bid": "0.00034089",
        "currency": "XPD",
        "pair": "USDXPD"
    },
    {
        "ask": "0.00082633",
        "bid": "0.00079335",
        "currency": "XPT",
        "pair": "USDXPT"
    },
    {
        "ask": "0.897949",
        "bid": "0.892132",
        "currency": "XRP",
        "pair": "USDXRP"
    },
    {
        "ask": "0.253176",
        "bid": "0.251046",
        "currency": "XTZ",
        "pair": "USD-XTZ"
    },
    {
        "ask": "7.5665859565",
        "bid": "7.4895146794",
        "currency": "ZIL",
        "pair": "USDZIL"
    },
    {
        "ask": "0.86598831",
        "bid": "0.85783893",
        "currency": "ZRX",
        "pair": "USDZRX"
    },
    {
        "ask": "1.0001",
        "bid": "0.9999",
        "currency": "USD",
        "pair": "USDC-USD"
    },
    {
        "ask": "1.00331",
        "bid": "0.99898",
        "currency": "USD",
        "pair": "USDT-USD"
    },
    {
        "ask": "228.83127",
        "bid": "224.30006",
        "currency": "USD",
        "pair": "V-USD"
    },
    {
        "ask": "0.00401",
        "bid": "0.00399",
        "currency": "USD",
        "pair": "VCO-USD"
    },
    {
        "ask": "0.00318",
        "bid": "0.00001",
        "currency": "USD",
        "pair": "VOXUSD"
    },
    {
        "ask": "40887.67991",
        "bid": "40488.53962",
        "currency": "USD",
        "pair": "WBTC-USD"
    },
    {
        "ask": "46.18845",
        "bid": "45.27384",
        "currency": "USD",
        "pair": "WFC-USD"
    },
    {
        "ask": "30.0883",
        "bid": "27.4899",
        "currency": "USD",
        "pair": "XAGUSD"
    },
    {
        "ask": "1913.82263",
        "bid": "1872.31028",
        "currency": "USD",
        "pair": "XAUUSD"
    },
    {
        "ask": "0.22536",
        "bid": "0.22106",
        "currency": "USD",
        "pair": "XEMUSD"
    },
    {
        "ask": "37.71434",
        "bid": "36.96753",
        "currency": "USD",
        "pair": "XLF-USD"
    },
    {
        "ask": "0.48024",
        "bid": "0.47594",
        "currency": "USD",
        "pair": "XLMUSD"
    },
    {
        "ask": "66.51015",
        "bid": "65.19314",
        "currency": "USD",
        "pair": "XLU-USD"
    },
    {
        "ask": "59.89449",
        "bid": "58.70848",
        "currency": "USD",
        "pair": "XOM-USD"
    },
    {
        "ask": "2933.41253",
        "bid": "2808.22494",
        "currency": "USD",
        "pair": "XPDUSD"
    },
    {
        "ask": "1260.47417",
        "bid": "1210.17504",
        "currency": "USD",
        "pair": "XPTUSD"
    },
    {
        "ask": "1.12091",
        "bid": "1.11365",
        "currency": "USD",
        "pair": "XRPUSD"
    },
    {
        "ask": "3.98332",
        "bid": "3.94983",
        "currency": "USD",
        "pair": "XTZ-USD"
    },
    {
        "ask": "0.13352",
        "bid": "0.13216",
        "currency": "USD",
        "pair": "ZILUSD"
    },
    {
        "ask": "1.16572",
        "bid": "1.15475",
        "currency": "USD",
        "pair": "ZRXUSD"
    }
]