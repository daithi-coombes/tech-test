#!/usr/env node

import config from './config/index.js'
import Alert from './lib/Alert.mjs'
import Poller from './lib/Poller.mjs'
import Uphold from './lib/Uphold.mjs'

const pairs = config.pairs.split(",").map(p => p.toUpperCase())
const client = new Uphold(config)
const poll = new Poller(pairs, client)

console.log('Endpoint: ', config.endpoint)
console.log('Environment: ', config.env)
console.log('Pairs: ', pairs)
console.log('Interval: ', config.interval+'ms')

let clear
clear = setInterval(async function doPoll(){
  try{
    for await (let p of poll.run()){ // @todo test n-notation, each request will block!
      if(p.percent>0.01){
        const pa = (p.bid>p.previousPrice) ? 'up' : 'down'
        Alert(p.bid, pa, p.percent, p.ticker)
      }
    }
  }catch(e){
    console.error(e)
    clearInterval(clear)
  }
}, config.interval)
