import https from 'https'

class Uphold{
  constructor(config){
      this.config = config
  }

  /**
   * Make a request for price data
   * @param {string} ticker The ticker for the relevant coin, can use any pair
   * @returns 
   */
  getTicker(ticker){
    ticker = ticker.toUpperCase()

    return new Promise((resolve, reject) => {
      const url = this.config.endpoint + "/v0/ticker/" + ticker

      https.get(url, res => {
        const {statusCode} = res
        res.setEncoding('utf8')

        let raw = ''
        res.on('data', (chunk) => { raw += chunk })

        res.on('end', () => {
          try {
            if(statusCode!=200) throw(raw)
            const j = JSON.parse(raw)
            resolve(j)
          } catch (e) {
            console.error(`error from ${url}:\n`, e)
            reject(e)
          }
        })
      })  // https.get()

    })
  }

}

export default Uphold