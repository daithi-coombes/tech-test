import Uphold from './Uphold.mjs'
import config from '../config/index.js'

const uphold = new Uphold(config)

export default class Poller{

  constructor(pairs, client){
    this.pairs = pairs
    this.client = client
  }
  previousPrice = {}

  /**
   * Run the iterable
   */
  async *run(){
    for (let i=0; i<this.pairs.length; i++){
      let data
      try{
        data = await this.client.getTicker(this.pairs[i])
      }catch(e){
        throw e
      }

      data.ticker = this.pairs[i]
      data.previousPrice = this.previousPrice[data.ticker]
      const delta = Poller.diff(data.bid, this.previousPrice[data.ticker])
      data.percent = parseFloat(delta / this.previousPrice[data.ticker] * 100).toFixed(2)

      this.previousPrice[data.ticker] = data.bid
      yield data
    }
  }

  /**
   * Get difference between two numbers.
   * 
   * @param {number} a First number to compare
   * @param {number} b Second number compare
   * @returns Returns the difference between the two numbers
   */
  static diff(a, b){
    return (a > b) ? Math.abs(a - b) : Math.abs(b - a);
  }    
}
