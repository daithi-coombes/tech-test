  /**
   * Will handle the alerting.
   * 
   * @param {number} price The current price
   * @param {string} pa The direction. enum(up, down)
   * @param {number} percent The percent change.
   */
   export default function Alert(price, pa, percent, ticker){
    const msg = `\tprice now ${price}\n\tprice ${pa} ${percent}%`
    console.log(`${ticker} ALERT!!!\n${msg}`)
}